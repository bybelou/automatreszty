package automat_2015_09_08;

import java.util.ArrayList;
import java.util.Collections;

class Automat{
	private static ArrayList<Monety> dostepneMonety = new ArrayList<Monety>();
	private static ArrayList<Monety> wrzuconeMonety = new ArrayList<Monety>();
	private int cenaZakupu;
	
	Automat(ArrayList<Monety> wm, int cz){
		deklarujDostepneMonety();

		Collections.sort(dostepneMonety);
		
		System.out.println("Dostepne monety w automacie");
		wypiszMonety();//monety znajdujace sie w automacie
		
		wrzuconeMonety = wm;
		
		wlozMonetyDoPojemnika();//wlozenie monet wrzuconych do automatu
		System.out.println("Dostepne monety w automacie po wrzuceniu monet");
		wypiszMonety();//wypisanie monet znajdujacych sie w automacie
		
		this.cenaZakupu = cz;
		
		wyznaczReszte();
		

		
		//wyjmijMonetyZPojemnika();//wyjecie monet wrzuconych do automatu
		//wypiszMonety();//wypisanie monet znajdujacych sie w automacie
	}

	private void deklarujDostepneMonety(){
		dostepneMonety.add(new Monety(500,1));
		dostepneMonety.add(new Monety(200,1));
		dostepneMonety.add(new Monety(100,1));
		dostepneMonety.add(new Monety(50,1));
		dostepneMonety.add(new Monety(20,1));
		dostepneMonety.add(new Monety(10,1));
		dostepneMonety.add(new Monety(5,1));
		dostepneMonety.add(new Monety(2,1));
		dostepneMonety.add(new Monety(1,1));
	}
	
	private void wypiszMonety(){
		System.out.println("Dostepne monety:");
		for (Monety moneta : dostepneMonety){
			System.out.println(moneta.toString());
		}
	}
	
	private void wlozMonetyDoPojemnika(){
		for (int i=0; i<wrzuconeMonety.size(); i++){//wszystkie wrzucone monety
			for (Monety moneta : dostepneMonety){//wszystkie dostepne monety w automacie
				if (wrzuconeMonety.get(i).pobierzKwoteMonety() == moneta.pobierzKwoteMonety()){
					for (int j=0; j<wrzuconeMonety.get(i).pobierzLiczbeMonet(); j++){
						moneta.wlozMonete();
					}
				}
			}
		}
	}
	private void wyjmijMonetyZPojemnika(){
		for (int i=0; i<wrzuconeMonety.size(); i++){//wszystkie wrzucone monety
			for (Monety moneta : dostepneMonety){//wszystkie dostepne monety w automacie
				if (wrzuconeMonety.get(i).pobierzKwoteMonety() == moneta.pobierzKwoteMonety()){
					for (int j=0; j<wrzuconeMonety.get(i).pobierzLiczbeMonet(); j++){
						moneta.pobierzMonete();
					}
				}
			}
		}
	}
	
	private void wyznaczReszte(){
		int temp = 0;
		ArrayList<Monety> reszta = new ArrayList<Monety>();
		reszta.add(new Monety(500,0));
		reszta.add(new Monety(200,0));
		reszta.add(new Monety(100,0));
		reszta.add(new Monety(50,0));
		reszta.add(new Monety(20,0));
		reszta.add(new Monety(10,0));
		reszta.add(new Monety(5,0));
		reszta.add(new Monety(2,0));
		reszta.add(new Monety(1,0));
		
		for (Monety moneta : wrzuconeMonety){
			for (int i=0; i<moneta.pobierzLiczbeMonet(); i++){
				temp += moneta.pobierzKwoteMonety();
			}
		}
		
		System.out.println("wrzucono:"+temp);
		
		temp -= this.cenaZakupu;
		
		System.out.println("do wydania:"+temp);
		
		for (Monety moneta : dostepneMonety){//wszystkie dostepne monety w automacie
			while (moneta.pobierzLiczbeMonet() > 0
					&& temp - moneta.pobierzKwoteMonety() >= 0){
				moneta.pobierzMonete();
				temp -= moneta.pobierzKwoteMonety();
				for (Monety mon : reszta){
					if (mon.pobierzKwoteMonety() == moneta.pobierzKwoteMonety()){
						mon.wlozMonete();
					}
				}
			}
		}
		
		if (temp == 0 ){
			for (Monety mon : reszta){
				System.out.println(mon);
			}
			System.out.println("wydano");
		}else{
			System.out.println("reszta");
			for (Monety moneta : reszta){
				System.out.println(moneta);
			}
			
			for (int i=0; i<reszta.size(); i++){//wszystkie wrzucone monety
				for (Monety moneta : dostepneMonety){//wszystkie dostepne monety w automacie
					if (reszta.get(i).pobierzKwoteMonety() == moneta.pobierzKwoteMonety()){
						for (int j=0; j<reszta.get(i).pobierzLiczbeMonet(); j++){
							moneta.wlozMonete();
							reszta.get(i).pobierzMonete();
						}
					}
				}
			}
			wyjmijMonetyZPojemnika();//wyjecie monet wrzuconych do automatu
			wypiszMonety();//wypisanie monet znajdujacych sie w automacie
			System.out.println("nie wydano");
			

			System.out.println("reszta");
			for (Monety moneta : reszta){
				System.out.println(moneta);
			}
		}
	}

}