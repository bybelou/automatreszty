package automat_2015_09_08;

class Monety implements Comparable<Monety>{
	private int nominal;
	private int licznik;
	
	Monety(int n, int l){
		this.nominal=n;
		this.licznik=l;
	}
	
	public boolean pobierzMonete(){
		if (licznik>0){
			this.licznik--;
			return true;
		}else{
			return false;
		}	
	}
	
	public void wlozMonete(){
		licznik++;
	}
	
	public int pobierzKwoteMonety(){
		return this.nominal;
	}
	
	public int pobierzLiczbeMonet(){
		return this.licznik;
	}
	
	@Override
	public String toString(){
		return "nominal:"+this.nominal+" licznik:"+this.licznik;
	}

	@Override
	public int compareTo(Monety o) {
		if (this.nominal < o.pobierzKwoteMonety()){
			return 1;
		}else if (this.nominal < o.pobierzKwoteMonety()){
			return -1;
		}else{
			return 0;
		}
	}
}