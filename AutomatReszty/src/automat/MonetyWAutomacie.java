package automat;

public class MonetyWAutomacie extends Monety {
	private int monetyWrzucone = 0;
	private int monetyWydane = 0;
	
	MonetyWAutomacie(int n, int l) {
		super(n, l);
		// TODO Auto-generated constructor stub
	}

	//pobiera monete o danej kwocie z automatu
	@Override
	boolean pobierzMonete() {
		// TODO Auto-generated method stub
		if (this.licznik>0){
			this.licznik--;
			return true;
		}else{
			return false;
		}
	}
	
	//wklada monete o danej kwocie z automatu
	@Override
	void wlozMonete() {
		// TODO Auto-generated method stub
		this.licznik++;
	}
	
	//wrzucenie monety do automatu do zakupu
	@Override
	void wrzucMonete() {
		// TODO Auto-generated method stub
		this.monetyWrzucone++;
	}
	
	//wydaje reszte po zakupie
	@Override
	boolean wydajMonete() {
		// TODO Auto-generated method stub
		if ( this.licznik+this.monetyWrzucone-this.monetyWydane > 0){
			this.monetyWydane++;
			return true;
		}else{
			return false;
		}
	}
	
	public void zatwierdzTransakcje(){
		this.licznik += this.monetyWrzucone;
		this.licznik -= this.monetyWydane;

		this.monetyWrzucone = 0;
		this.monetyWydane = 0;
	}
	
	public int wycofajTransakcje(){
		int temp = this.monetyWrzucone;
		
		this.monetyWrzucone = 0;
		this.monetyWydane = 0;
		
		return temp;
	}
	
	public int pobierzSumeWrzuconychMonetOWalucie(){
		return this.monetyWrzucone * this.nominal;
	}
	
	@Override
	public String toString(){
		return "nominal:"+this.nominal+" licznik:" + (this.licznik + this.monetyWrzucone - this.monetyWydane)
				+ "\n\tnominal:" + this.nominal + " licznik:" + this.licznik + " wrzucone:" + this.monetyWrzucone + " wydane:" + this.monetyWydane;
	}
}
