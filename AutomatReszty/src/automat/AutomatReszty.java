package automat;

import java.util.ArrayList;

public class AutomatReszty {

	public static void main(String[] args) {
		final ArrayList<MonetyWAutomacie> dostepneMonety = new ArrayList<MonetyWAutomacie>();
		
		dostepneMonety.add(new MonetyWAutomacie(500,1));
		dostepneMonety.add(new MonetyWAutomacie(200,1));
		dostepneMonety.add(new MonetyWAutomacie(100,1));
		dostepneMonety.add(new MonetyWAutomacie(50,1));
		dostepneMonety.add(new MonetyWAutomacie(20,1));
		dostepneMonety.add(new MonetyWAutomacie(10,1));
		dostepneMonety.add(new MonetyWAutomacie(5,1));
		dostepneMonety.add(new MonetyWAutomacie(2,1));
		dostepneMonety.add(new MonetyWAutomacie(1,1));
		
		Automat automat = new Automat(dostepneMonety);

		System.out.println(automat.wrzucMonete(500));
		System.out.println(automat.wrzucMonete(10));
		System.out.println(automat.wrzucMonete(10));
		System.out.println(automat.wrzucMonete(22));
		
		automat.wypiszMonety();
	}
}