package automat;

import java.util.ArrayList;
import java.util.Collections;

class Automat{
	private ArrayList<MonetyWAutomacie> dostepneMonety = new ArrayList<MonetyWAutomacie>();
	private int cenaZakupu;
	
	Automat(ArrayList<MonetyWAutomacie> wm){
		this.dostepneMonety = wm;
		Collections.sort(dostepneMonety);
		
		System.out.println("Dostepne monety w automacie");
		wypiszMonety();//monety znajdujace sie w automacie
	}
	
	public void wypiszMonety(){
		System.out.println("Dostepne monety:");
		for (MonetyWAutomacie moneta : dostepneMonety){
			System.out.println(moneta.toString());
		}
	}
	
	public String wrzucMonete(int nominal){
		boolean temp = false;
		for (MonetyWAutomacie moneta : dostepneMonety){
			if (moneta.pobierzKwoteMonety() == nominal){
				moneta.wrzucMonete();
				temp = true;
			}
		}
		if (temp){
			return "Wrzucony kredyt: " + zsumujWrzuconeMonety();
		}else{
			return "Bledna moneta";
		}
	}
	
	public int zsumujWrzuconeMonety(){
		int suma = 0;
		for (MonetyWAutomacie moneta : dostepneMonety){
			suma += moneta.pobierzSumeWrzuconychMonetOWalucie();
		}
		return suma;
	}
	
	public boolean realizujZakup(int cena){
		boolean statusTransakcji = false;
		this.cenaZakupu = cena;
		
		
		
		if (statusTransakcji){
			for (MonetyWAutomacie moneta : dostepneMonety){
				moneta.zatwierdzTransakcje();
			}	
		}else{
			for (MonetyWAutomacie moneta : dostepneMonety){
				moneta.wycofajTransakcje();
			}
		}
		return statusTransakcji;
	}
	
	/*private void wyznaczReszte(){
		int temp = 0;
		for (Monety moneta : wrzuconeMonety){
			for (int i=0; i<moneta.pobierzLiczbeMonet(); i++){
				temp += moneta.pobierzKwoteMonety();
			}
		}
		
		System.out.println("wrzucono:"+temp);
		
		temp -= this.cenaZakupu;
		
		System.out.println("do wydania:"+temp);
	}*/
}