package automat;

abstract class Monety implements Comparable<Monety>{
	protected int nominal;
	protected int licznik;
	
	Monety(int n, int l){
		this.nominal=n;
		this.licznik=l;
	}
	
	abstract boolean pobierzMonete();
	
	abstract void wlozMonete();
	
	abstract boolean wydajMonete(); 
	
	abstract void wrzucMonete();
	
	
	public int pobierzKwoteMonety(){
		return this.nominal;
	}
	
	public int pobierzLiczbeMonet(){
		return this.licznik;
	}
	
	@Override
	public String toString(){
		return "nominal:"+this.nominal+" licznik:"+this.licznik;
	}

	@Override
	public int compareTo(Monety o) {
		if (this.nominal > o.pobierzKwoteMonety()){
			return 1;
		}else if (this.nominal < o.pobierzKwoteMonety()){
			return -1;
		}else{
			return 0;
		}
	}
}